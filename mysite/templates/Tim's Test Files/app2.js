var weatherApp = angular.module('weatherApp', ['ngRoute', 'ngResource']);

weatherApp.config(function ($routeProvider) {

    $routeProvider

    .when('/weatherHome', {
        templateUrl: 'weatherHome.html',
        controller: 'homeController'
    })

    .when('/weatherForecast', {
        templateUrl: 'weatherForecast.html',
        controller: 'forecastController'
    })

});

weatherApp.service('cityService', function() {

    this.city = "New York, NY";


});

weatherApp.controller('homeController', ['$scope','cityService', function($scope, cityService) {

    $scope.city = cityService.city;
    $scope.$watch('city', function() {
        cityService.city = $scope.city;
    });

}]);

weatherApp.controller('forecastController', ['$scope', 'cityService', '$resource', function($scope, cityService, $resource) {

    $scope.city = cityService.city;

    $scope.weatherAPI = $resource("http://api.openweathermap.org/data/2.5/forecast/daily", { callback: "JSON_CALLBACK" }, { get: { method: "JSONP" }});

    $scope.weatherResult = $scope.weatherAPI.get({ q: $scope.city, cnt: $scope.days });

    console.log($scope.weatherResult);

}]);

