from flask import Flask
from flask import request
from flask import render_template
from flask import redirect
from flask import session, url_for, escape
from flask.ext.sqlalchemy import SQLAlchemy
import requests

app = Flask(__name__)
app.debug = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:tzabawa@localhost/tzabawa'
db = SQLAlchemy(app)

UserPlayer = db.Table('UserPlayer',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('player_id', db.Integer, db.ForeignKey('player.id'))
)

class PlayerSchedule(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    player = db.Column(db.String(30))
    date = db.Column(db.String(20))
    opponent = db.Column(db.String(50))
    status = db.Column(db.String(10))

    def __init__(self, player, date, opponent, status):
        self.player = player
        self.date = date
        self.opponent = opponent
        self.status = status

    def __repr__(self):
        return '<PlayerSchedule %r>' % self.player

class PlayerPost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(40))
    vote = db.Column(db.String(10))
    reason = db.Column(db.String(200))
    player = db.Column(db.String(80))
    picture = db.Column(db.String(200))

    def __init__(self, username, vote, reason, player, picture):
        self.username = username
        self.vote = vote
        self.reason = reason
        self.player = player
        self.picture = picture

    def __repr__(self):
        return '<PlayerPost %r>' % self.username

class Player(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    player = db.Column(db.String(80), unique=True)
    picture = db.Column(db.String(200),unique=True)

    def __init__(self, player, picture):
        self.player = player
        self.picture = picture

    def __repr__(self):
        return '<Player %r>' % self.player

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(40), unique=True)
    password = db.Column(db.String(40))
    email = db.Column(db.String(40), unique=True)
    players = db.relationship('Player', secondary=UserPlayer, \
    backref=db.backref('user', lazy='dynamic'))

    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email

    def __repr__(self):
        return '<User %r>' % self.username

from app import db
from app import PlayerSchedule
from app import PlayerPost
from app import User
from app import Player
from app import UserPlayer
db.create_all()

@app.route('/')
def hello_world1():
        # Displaying the 'home_page' in the browser window.
        if session == {}:
            username = ""
            return render_template('Home.html', username=username)

        # Redirecting the user to their "fantasy_team page" if they are logged in.
        else:
            username = session['username']
            return redirect(url_for('hello_world13'))

@app.route('/UserPost/<username>')
def hello_world2(username):
        # Displaying the selected user's 'UserPost_page' in the browser window.
        user_posts = PlayerPost.query.filter_by(username=username).all()
        user_posts_username = username
        username = session['username']
        return render_template('UserPost.html', username=username, user_posts=user_posts, user_posts_username=user_posts_username)

@app.route('/CreateAccount', methods=['POST', 'GET'])
def hello_world3():
        # Creating a user object and adding it to the User database.
        if request.method == 'POST':
            username = request.form['username']
            email = request.form['email']
            user_verification = User.query.filter_by(username=username).first()
            email_verification = User.query.filter_by(email=email).first()

            # Checking to see if the username or email entered already exists in the database and what to do in both scenarios.
            if user_verification or email_verification is not None:
                error = "The username or email address entered has been registered to another user, please try again!"
                username= ""
                return render_template('CreateAccount.html', username=username, error=error)
            else:
                user_object = User(request.form['username'], request.form['password'], request.form['email'])
                db.session.add(user_object)
                db.session.commit()
                session['username'] = request.form['username']
                username = session['username']
                return redirect('/FantasyTeam')

        # Displaying the "CreateAccount_page" in the browser window.
        else:
            username = ""
            error = ""
            return render_template('CreateAccount.html', username=username, error=error)

@app.route('/Sent')
def hello_world4():
        # Displaying the 'Sent_page' in the browser window.
        session == {}
        username = ""
        return render_template('Sent.html', username=username)

@app.route('/Forgot', methods=['POST', 'GET'])
def hello_world5():
        # Sending an email (content = user's username and password) to user.
        if request.method == 'POST':
            user_email = request.form['email']
            queried_user_object = User.query.filter_by(email=user_email).first()

            # Verifying if the email entered exists in the database.
            if queried_user_object is not None:
                user_email_attribute = queried_user_object.email
                user_username_attribute = queried_user_object.username
                user_password_attribute = queried_user_object.password
                username = "Your Username is: " + user_username_attribute + "."
                username_and_password = username + " Your Password is: " + user_password_attribute + "."
                requests.post(
                        "https://api.mailgun.net/v2/tostartorsit.com/messages",
                        auth=("api", "key-55c438d4f58daec65e6e37742dd2c63e"),
                        data={"from": "<tzabawa@tostartorsit.com>",
                        "to": [user_email_attribute],
                        "subject": "Username and Password",
                        "text": username_and_password})
                return redirect('/Sent')
            else:
                session == {}
                username = ""
                error = 'The email you entered was not found in the database, please try again!'
                return render_template('Forgot.html', username=username, error=error)

        # Displaying the 'Forgot_page' in the browser window.
        else:
            session == {}
            username = ""
            error = ""
            return render_template('Forgot.html', username=username, error=error)

@app.route('/LogIn', methods=['POST', 'GET'])
def hello_world6():
        # Logging in with the user's username and password.
        if request.method == 'POST':
            username = request.form['username']
            queried_user_object = User.query.filter_by(username=username).first()

            # Checking to see if the username entered exists in the database and what to do in both scenarios.
            if queried_user_object is not None:
                username_attribute = queried_user_object.username

                # Checking to see if the username entered matches the queried username attribute and what to do in both scenarios.
                if username == username_attribute:
                    session['username'] = username
                    return redirect('/FantasyTeam')
                else:
                    error = 'Your username or password was entered incorrectly, try again!'
                    username = ""
                    return render_template('LogIn.html', username=username, error=error)
            else:
                error = 'Your username or password was entered incorrectly, please try again!'
                username = ""
                return render_template('LogIn.html', username=username, error=error)

        # Displaying the 'LogIn_page' in the browser window.
        else:
            username = ""
            return render_template('LogIn.html', username=username)

@app.route('/PlayerSearch', methods=['POST', 'GET'])
def hello_world7():
        # Searching for a player's voting page in the search bar and redirecting the user to his page.
        if request.method == 'POST':
            players_name = request.form['players_name']
            player = Player.query.filter_by(player=players_name).first()

            # Verifying the player exists in the database.
            if player is not None:
                player_id_attribute = player.id
                player_id_attribute_string = str(player_id_attribute)
                Players_Vote = '/PlayerVote/'
                player_and_id = Players_Vote + player_id_attribute_string
                return redirect(player_and_id)
            else:
                username = session['username']
                queried_user_object = User.query.filter_by(username=username).first()
                users_players_list = queried_user_object.players
                return render_template('FantasyTeam.html', username=username, users_players_list=users_players_list)

        else:
            # Displaying the user's 'FantasyTeam_page' in the browser window.
            username = session['username']
            queried_user_object = User.query.filter_by(username=username).first()
            users_players_list = queried_user_object.players
            return render_template('FantasyTeam.html', username=username, users_players_list=users_players_list)

@app.route('/PlayerVote/<id_attribute>')
def hello_world8(id_attribute):
        # Displaying the chosen player's 'PlayerVote_page' in the browser window.
        username = session['username']
        player = Player.query.filter_by(id=id_attribute).first()
        players_name_attribute = player.player
        players_picture_attribute = player.picture
        players_id_attribute = player.id
        id_attribute_string = str(players_id_attribute)
        Players_Start_Page = '/Start/'
        Player_and_ID_Start = Players_Start_Page + id_attribute_string
        Players_Sit_Page = '/Sit/'
        Player_and_ID_Sit = Players_Sit_Page + id_attribute_string

        # Displaying the player's game date, opponent, and home/away status.
        player_schedule = PlayerSchedule.query.filter_by(player=players_name_attribute).first()
        game_date = player_schedule.date
        game_opponent = player_schedule.opponent
        game_status = player_schedule.status
        start_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Start').all()
        start_count = len(start_posts)
        sit_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Sit').all()
        sit_count = len(sit_posts)
        return render_template('PlayerVote.html', username=username, players_name_attribute=players_name_attribute, players_picture_attribute=players_picture_attribute, players_id_attribute=players_id_attribute, Player_and_ID_Start=Player_and_ID_Start, Player_and_ID_Sit=Player_and_ID_Sit, game_date=game_date, game_opponent=game_opponent, game_status=game_status, start_count=start_count, sit_count=sit_count)

@app.route('/Sit/<players_id_attribute>', methods=['POST', 'GET'])
def hello_world9(players_id_attribute):
        # Displaying the player's 'Sit_page" in the browser window. This is where users can view a player's "Sit" votes and reasons. The user can also enter a single vote themselves.
        if request.method == 'POST':
            username = session['username']
            vote = request.form['vote']
            reason = request.form["reason"]
            player = Player.query.filter_by(id=players_id_attribute).first()
            players_name_attribute = player.player
            post_list_verification = PlayerPost.query.filter_by(username=username, player=players_name_attribute, vote='Sit').first()

            # Verifying that a user can only make one vote per player.
            if post_list_verification is not None:
                players_picture_attribute = player.picture
                sit_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Sit').all()
                start_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Start').all()
                start_count = len(start_posts)
                sit_count = len(sit_posts)
                players_id_attribute = player.id
                id_attribute_string = str(players_id_attribute)
                Players_Start_Page = '/Start/'
                Player_and_ID_Start = Players_Start_Page + id_attribute_string
                Players_Sit_Page = '/Sit/'
                Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
                # Displaying the player's game date, opponent, and home/away status.
                player_schedule = PlayerSchedule.query.filter_by(player=players_name_attribute).first()
                game_date = player_schedule.date
                game_opponent = player_schedule.opponent
                game_status = player_schedule.status
                return render_template('Sit.html', username=username, players_name_attribute=players_name_attribute, players_picture_attribute=players_picture_attribute, players_id_attribute=players_id_attribute, sit_posts=sit_posts, Player_and_ID_Start=Player_and_ID_Start, Player_and_ID_Sit=Player_and_ID_Sit, game_date=game_date, game_opponent=game_opponent, game_status=game_status, start_count=start_count, sit_count=sit_count)
            else:
                player_picture_attribute = player.picture
                player_post = PlayerPost(username, vote, reason, players_name_attribute, player_picture_attribute)
                db.session.add(player_post)
                db.session.commit()
                sit_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Sit').all()
                start_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Start').all()
                start_count = len(start_posts)
                sit_count = len(sit_posts)
                players_id_attribute = player.id
                id_attribute_string = str(players_id_attribute)
                Players_Start_Page = '/Start/'
                Player_and_ID_Start = Players_Start_Page + id_attribute_string
                Players_Sit_Page = '/Sit/'
                Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
               # Displaying the player's game date, opponent, and home/away status.
                player_schedule = PlayerSchedule.query.filter_by(player=players_name_attribute).first()
                game_date = player_schedule.date
                game_opponent = player_schedule.opponent
                game_status = player_schedule.status
                return render_template('Sit.html', username=username, players_name_attribute=players_name_attribute,  players_picture_attribute= players_picture_attribute, players_id_attribute=players_id_attribute, sit_posts=sit_posts, Player_and_ID_Start=Player_and_ID_Start, Player_and_ID_Sit=Player_and_ID_Sit, game_date=game_date, game_opponent=game_opponent, game_status=game_status, start_count=start_count, sit_count=sit_count)

        else:
            # Displaying the chosen player's 'Sit_page' in the browser window.
            username = session['username']
            player = Player.query.filter_by(id=players_id_attribute).first()
            players_name_attribute = player.player
            players_picture_attribute = player.picture
            sit_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Sit').all()
            start_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Start').all()
            start_count = len(start_posts)
            sit_count = len(sit_posts)
            players_id_attribute = player.id
            id_attribute_string = str(players_id_attribute)
            Players_Start_Page = '/Start/'
            Player_and_ID_Start = Players_Start_Page + id_attribute_string
            Players_Sit_Page = '/Sit/'
            Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
            # Displaying the player's game date, opponent, and home/away status.
            player_schedule = PlayerSchedule.query.filter_by(player=players_name_attribute).first()
            game_date = player_schedule.date
            game_opponent = player_schedule.opponent
            game_status = player_schedule.status
            if sit_posts is not None:
                return render_template('Sit.html', username=username, players_name_attribute=players_name_attribute, players_picture_attribute= players_picture_attribute, players_id_attribute=players_id_attribute, sit_posts=sit_posts, Player_and_ID_Start=Player_and_ID_Start, Player_and_ID_Sit=Player_and_ID_Sit, game_date=game_date, game_opponent=game_opponent, game_status=game_status, start_count=start_count, sit_count=sit_count)

            else:
                sit_posts = {}
                player = Player.query.filter_by(id=players_id_attribute).first()
                players_name_attribute = player.player
                players_picture_attribute = player.picture
                players_id_attribute = player.id
                id_attribute_string = str(players_id_attribute)
                Players_Start_Page = '/Start/'
                Player_and_ID_Start = Players_Start_Page + id_attribute_string
                Players_Sit_Page = '/Sit/'
                Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
                sit_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Sit').all()
                start_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Start').all()
                start_count = len(start_posts)
                sit_count = len(sit_posts)
                return render_template('Sit.html', username=username, players_name_attribute=players_name_attribute,  players_picture_attribute= players_picture_attribute, players_id_attribute=players_id_attribute, sit_posts=sit_posts, Player_and_ID_Start=Player_and_ID_Start, Player_and_ID_Sit=Player_and_ID_Sit, game_date=game_date, game_opponent=game_opponent, game_status=game_status, start_count=start_count, sit_count=sit_count, Sit=Sit)

@app.route('/Start/<players_id_attribute>', methods=['POST', 'GET'])
def hello_world11(players_id_attribute):
        # Displaying the player's 'Start_page" in the browser window. This is where users can view a player's "Start" votes and reasons. The user can also enter a single vote themselves.
        if request.method == 'POST':
            username = session['username']
            vote = request.form['vote']
            reason = request.form["reason"]
            player = Player.query.filter_by(id=players_id_attribute).first()
            players_name_attribute = player.player
            post_list_verification = PlayerPost.query.filter_by(username=username, player=players_name_attribute, vote='Start').first()

            # Verifying that a user can only make one vote per player.
            if post_list_verification is not None:
                players_picture_attribute = player.picture
                sit_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Sit').all()
                start_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Start').all()
                start_count = len(start_posts)
                sit_count = len(sit_posts)
                players_id_attribute = player.id
                id_attribute_string = str(players_id_attribute)
                Players_Start_Page = '/Start/'
                Player_and_ID_Start = Players_Start_Page + id_attribute_string
                Players_Sit_Page = '/Sit/'
                Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
                # Displaying the player's game date, opponent, and home/away status.
                player_schedule = PlayerSchedule.query.filter_by(player=players_name_attribute).first()
                game_date = player_schedule.date
                game_opponent = player_schedule.opponent
                game_status = player_schedule.status
                return render_template('Start.html', username=username, players_name_attribute=players_name_attribute,  players_picture_attribute= players_picture_attribute, players_id_attribute=players_id_attribute, start_posts=start_posts, Player_and_ID_Start=Player_and_ID_Start, Player_and_ID_Sit=Player_and_ID_Sit, game_date=game_date, game_opponent=game_opponent, game_status=game_status, start_count=start_count, sit_count=sit_count)
            else:
                player = Player.query.filter_by(id=players_id_attribute).first()
                player_picture_attribute = player.picture
                player_post = PlayerPost(username, vote, reason, players_name_attribute, player_picture_attribute)
                db.session.add(player_post)
                db.session.commit()
                sit_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Sit').all()
                players_picture_attribute = player.picture
                start_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Start').all()
                start_count = len(start_posts)
                sit_count = len(sit_posts)
                players_id_attribute = player.id
                id_attribute_string = str(players_id_attribute)
                Players_Start_Page = '/Start/'
                Player_and_ID_Start = Players_Start_Page + id_attribute_string
                Players_Sit_Page = '/Sit/'
                Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
                # Displaying the player's game date, opponent, and home/away status.
                player_schedule = PlayerSchedule.query.filter_by(player=players_name_attribute).first()
                game_date = player_schedule.date
                game_opponent = player_schedule.opponent
                game_status = player_schedule.status
                return render_template('Start.html', username=username, players_name_attribute=players_name_attribute, players_picture_attribute=players_picture_attribute, players_id_attribute=players_id_attribute, start_posts=start_posts, Player_and_ID_Start=Player_and_ID_Start, Player_and_ID_Sit=Player_and_ID_Sit, game_date=game_date, game_opponent=game_opponent, game_status=game_status, start_count=start_count, sit_count=sit_count)

        else:
            # Displaying the chosen player's 'Start_page' in the browser window.
            username = session['username']
            player = Player.query.filter_by(id=players_id_attribute).first()
            players_name_attribute = player.player
            players_picture_attribute = player.picture
            sit_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Sit').all()
            start_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Start').all()
            start_count = len(start_posts)
            sit_count = len(sit_posts)
            players_id_attribute = player.id
            id_attribute_string = str(players_id_attribute)
            Players_Start_Page = '/Start/'
            Player_and_ID_Start = Players_Start_Page + id_attribute_string
            Players_Sit_Page = '/Sit/'
            Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
            # Displaying the player's game date, opponent, and home/away status.
            player_schedule = PlayerSchedule.query.filter_by(player=players_name_attribute).first()
            game_date = player_schedule.date
            game_opponent = player_schedule.opponent
            game_status = player_schedule.status
            if start_posts is not None:
                 return render_template('Start.html', username=username, players_name_attribute=players_name_attribute,  players_picture_attribute= players_picture_attribute, players_id_attribute=players_id_attribute, start_posts=start_posts, Player_and_ID_Start=Player_and_ID_Start, Player_and_ID_Sit=Player_and_ID_Sit, game_date=game_date, game_opponent=game_opponent, game_status=game_status, start_count=start_count, sit_count=sit_count)

            else:
                start_posts = {}
                player = Player.query.filter_by(id=players_id_attribute).first()
                players_name_attribute = player.player
                players_picture_attribute = player.picture
                players_id_attribute = player.id
                id_attribute_string = str(players_id_attribute)
                Players_Start_Page = '/Start/'
                Player_and_ID_Start = Players_Start_Page + id_attribute_string
                Players_Sit_Page = '/Sit/'
                Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
                sit_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Sit').all()
                start_posts = PlayerPost.query.filter_by(player=players_name_attribute, vote='Start').all()
                start_count = len(start_posts)
                sit_count = len(sit_posts)
                return render_template('Start.html', username=username, players_name_attribute=players_name_attribute,  players_picture_attribute= players_picture_attribute, players_id_attribute=players_id_attribute, start_posts=start_posts, Player_and_ID_Start=Player_and_ID_Start, Player_and_ID_Sit=Player_and_ID_Sit, game_date=game_date, game_opponent=game_opponent, game_status=game_status, start_count=start_count, sit_count=sit_count)

@app.route('/Remove/<id_attribute>')
def hello_world12(id_attribute):
        # Deleting a player from a user's list of players and displaying the altered list on the screen.
        username = session['username']
        queried_user_object = User.query.filter_by(username=username).first()
        queried_player_object = Player.query.filter_by(id=id_attribute).first()
        db.session.add(queried_player_object)
        queried_user_object.players.remove(queried_player_object)
        db.session.commit()
        users_players_list = queried_user_object.players
        return render_template('FantasyTeam.html', username=username, users_players_list=users_players_list)

@app.route('/FantasyTeam', methods=['POST', 'GET'])
def hello_world13():
        # Adding a player to a user's list of players and displaying them on the screen.
        if request.method == 'POST':
            username = session['username']
            users_player = request.form['Add']
            queried_user_object = User.query.filter_by(username=username).first()
            queried_player_object = Player.query.filter_by(player=users_player).first()
            users_players_list = queried_user_object.players

            # Checking to see if the player is already in the user's list of players.
            for players in users_players_list:
                players_name_attribute = players.player
                if players_name_attribute == users_player or players_name_attribute.lower() == users_player:
                    error = "The player you have entered has already been added to your team."
                    return render_template('FantasyTeam.html', username=username, users_players_list=users_players_list, error=error)

            # Checking to see if the user's list of players is empty or not and what to do with the player entry in both scenarios.
            if queried_player_object is not None:
                db.session.add(queried_player_object)
                if queried_user_object.players == {}:
                    queried_user_object.players = [queried_player_object]
                    db.session.commit()
                    users_players_list = queried_user_object.players
                    error = ""
                    return render_template('FantasyTeam.html', username=username, users_players_list=users_players_list, error=error)
                else:
                    queried_user_object.players.append(queried_player_object)
                    db.session.commit()
                    users_players_list = queried_user_object.players
                    error = ""
                    return render_template('FantasyTeam.html', username=username, users_players_list=users_players_list, error=error)

            else:
                users_players_list = queried_user_object.players
                error = "Sorry, that player or defense is not in the database."
                return render_template('FantasyTeam.html', username=username, users_players_list=users_players_list, error=error)

        elif session == {}:
            return redirect('/')
        else:
            username = session['username']
            queried_user_object = User.query.filter_by(username=username).first()
            users_players_list = queried_user_object.players
            error = ""
            return render_template('FantasyTeam.html', username=username, users_players_list=users_players_list, error=error)

@app.route('/LogOut')
def hello_world14():
        # Exiting a user's session.
        session.pop('username')
        return redirect(url_for('hello_world1'))

@app.route('/Delete/Sit/<players_id_attribute>')
def hello_world15(players_id_attribute):
        # Deleting a player's 'Sit' post.
        username = session['username']
        player = Player.query.filter_by(id=players_id_attribute).first()
        players_player_attribute = player.player
        deleted_post = PlayerPost.query.filter_by(username=username, player=players_player_attribute, vote='Sit').first()
        db.session.delete(deleted_post)
        db.session.commit()
        player = Player.query.filter_by(id=players_id_attribute).first()
        players_picture_attribute = player.picture
        players_id_attribute = player.id
        id_attribute_string = str(players_id_attribute)
        Players_Start_Page = '/Start/'
        Player_and_ID_Start = Players_Start_Page + id_attribute_string
        Players_Sit_Page = '/Sit/'
        Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
        return redirect(Player_and_ID_Sit)

@app.route('/Delete/Start/<players_id_attribute>')
def hello_world16(players_id_attribute):
        # Deleting a player's 'Start' post.
        username = session['username']
        player = Player.query.filter_by(id=players_id_attribute).first()
        players_player_attribute = player.player
        deleted_post = PlayerPost.query.filter_by(username=username, player=players_player_attribute, vote='Start').first()
        db.session.delete(deleted_post)
        db.session.commit()
        player = Player.query.filter_by(id=players_id_attribute).first()
        players_picture_attribute = player.picture
        players_id_attribute = player.id
        id_attribute_string = str(players_id_attribute)
        Players_Start_Page = '/Start/'
        Player_and_ID_Start = Players_Start_Page + id_attribute_string
        Players_Sit_Page = '/Sit/'
        Player_and_ID_Sit = Players_Sit_Page + id_attribute_string
        return redirect(Player_and_ID_Start)

@app.route('/TestPage')
def hello_world17():
        # Displaying a page where I could test my different programs.
        if session == {}:
            username = ""
            return render_template('TestPage.html', username=username)
        else:
            username = session['username']
            return render_template('TestPage.html', username=username)

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

if __name__ == '__main__':
	app.run(host='0.0.0.0')
