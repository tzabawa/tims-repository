// MODULE
var angularApp = angular.module('angularApp', ['ngRoute']);

angularApp.config(function ($routeProvider){


    $routeProvider

    .when('/Index', {
        templateUrl: 'main.html',
        controller: 'mainController'
})


});

// CONTROLLERS
angularApp.controller('mainController', ['$scope', '$filter', '$location', '$log', function ($scope, $filter, $location, $log) {

$scope.people = [{
    name: 'John Doe',
    address: '555 Main St.',
    city: 'New York',
},
{
    name: 'Jane Doe',
    address: '555 Main St.',
    city: 'New York',
},
{
    name: 'Mark Doe',
    address: '22 Bogg St.',
    city: 'Pennsylvania',
}]

$scope.addressCreator = function(person) {
    return person.address + ', ' + person.city;
}

$scope.handle = '';
$scope.lowercasehandle = function () {
return $filter('lowercase')($scope.handle);
};

$scope.characters = 5;

$scope.rules = [
{rulename: "Must be 5 characters" },
{rulename: "Must not be used elsewhere" },
{rulename: "It must be cool" }
];

$scope.alertClick = function() {
alert("Clicked");
}
console.log($scope.rules);

}]);

angularApp.directive("searchResult", function() {
    return {
        templateUrl: 'directivesExample.html',
        replace: true,
        scope: {
            personObject: '=',
            addressCreator: '&'


        }
    }



});
